@extends('userdashboard::layouts.master')

@section('content')
    <div class="container">
        <hr>
        <h5>Создание кода игры</h5>
        <form class="" action="/gameframe/store-user-frame" method="post">
            @csrf
            <div class="row">
                <div class="col-3">
                    <b>Адрес сайта</b>
                </div>
                <div class="col-6">
                    <input type="text" name="url" value="">
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-3">
                    <b>Код игры</b>
                </div>
                <div class="col-6">
                    <input type="text" name="code" value="">
                </div>
            </div> -->
            <input type="submit" name="" value="Создать">
        </form>
        <hr>
        <div class="">
            <span style="color: red">{{$error}}</span>
        </div>
    </div>

@endsection
