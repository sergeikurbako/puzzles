@extends('userdashboard::layouts.master')

@section('content')

    <div class="container">
        <hr>
        <form class="" action="/user-dashboard/frame-rules/{{$frameId}}/update" method="post">
            @csrf

            <input type="text" name="mazeWidth" value="{{$mazeWidth}}" placeholder="Ширина лабиринта"> Ширина лабиринта<br>
            <input type="text" name="mazeHeight" value="{{$mazeHeight}}" placeholder="Высота лабиринта"> Высота лабиринта<br>
            <input type="text" name="cameraWidth" value="{{$cameraWidth}}" placeholder="Ширина камеры"> Ширина камеры<br>
            <input type="text" name="cameraHeight" value="{{$cameraHeight}}" placeholder="Высота камеры"> Высота камеры<br>
            <input type="text" name="countOfTimeBonus" value="{{$countOfTimeBonus}}" placeholder="Кол-во бонусов времени"> Кол-во бонусов времени<br>
            <input type="text" name="countOfHealthBonus" value="{{$countOfHealthBonus}}" placeholder="Кол-во бонусов жизней"> Кол-во бонусов жизней<br>
            <input type="text" name="decreaseTime" value="{{$decreaseTime}}" placeholder="Скорость уменьшения времени"> Скорость уменьшения времени<br>
            <input type="text" name="speed" value="{{$speed}}" placeholder="Скорость героя"> Скорость героя<br>
            <input type="text" name="health" value="{{$health}}" placeholder="Жизни героя"> Жизни героя<br>
            <input type="text" name="time" value="{{$time}}" placeholder="Время героя"> Время героя<br>
            <input type="text" name="botSpeed" value="{{$botSpeed}}" placeholder="Скорость врагов"> Скорость врагов<br><br>
            <input type="submit" name="" value="Применить">

        </form>

    </div>
@endsection
