@extends('userdashboard::layouts.master')

@section('content')
    <div class="container">
        <hr>
        <h5>Редактирование фрейма</h5>
        <form class="" action="/user-dashboard/frame/{{$frame->id}}/update" method="post">
            @csrf
            <div class="row">
                <div class="col-1">
                    Сайт:
                </div>
                <div class="col-6">
                    <input type="text" name="url" value="{{$frame->url}}">
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    Код фрейма:
                </div>
                <div class="col-6">
                    <input type="text" name="code" value="{{$frame->code}}">
                </div>
            </div>
            <br>
            <input type="submit" name="" value="Применить">
        </form>
    </div>

@endsection
