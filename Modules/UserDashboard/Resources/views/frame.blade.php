@extends('userdashboard::layouts.master')

@section('content')
    <div class="container">
        <hr>
        <div class="row">
            <form class="" action="/user-dashboard/frame/{{$frameId}}" method="get">
                <table>
                    <thead>
                        <th>
                            <td><b>Фильтр:</b></td>
                            <td>Дата</td>
                            <td>с <input type="date" name="from_date" value=""></td>
                            <td>по <input type="date" name="to_date" value=""></td>
                            <td><select name="gender">
                                <option value="man">муж</option>
                                <option value="waman">жен</option>
                            </select></td>
                            <td><input type="submit" name="" value="применить"></td>
                        </th>
                    </thead>
                </table>
            </form>

        </div>
        <hr>
        <div class="row">
            <div class="col-1">
                №
            </div>
            <div class="col-1">
                Дата
            </div>
            <div class="col-1">
                ФИО
            </div>
            <div class="col-1">
                Пол
            </div>
            <div class="col-1">
                Возраст
            </div>
            <div class="col-2">
                E-mail
            </div>
            <div class="col-2">
                Номер телефона
            </div>
            <div class="col-1">
                Цена лида
            </div>
            <div class="col-1">
                Результат игры
            </div>
            <div class="col-1">
                Пожаловаться модератору
            </div>
        </div>
        @foreach($lids as $lid)
            <hr>
            <div class="row">
                <div class="col-1">
                    {{$lid->id}}
                </div>
                <div class="col-1">
                    {{$lid->created_at}}
                </div>
                <div class="col-1">
                    {{$lid->second_name}} {{$lid->first_name}} {{$lid->patronymic_name}}
                </div>
                <div class="col-1">
                    @if($lid->gender === 'man') муж @else жун @endif
                </div>
                <div class="col-1">
                    {{$lid->age}}
                </div>
                <div class="col-2">
                    {{$lid->email}}
                </div>
                <div class="col-2">
                    {{$lid->phone}}
                </div>
                <div class="col-1">
                    {{$lid->price}}
                </div>
                <div class="col-1">
                    @if ($lid->session_id !== 0)
                        <a href="/maze?session_id={{$lid->session_id}}">{{$lid->game_result}}</a>
                    @else
                        {{$lid->game_result}}
                    @endif
                </div>
                <div class="col-1">
                    @if ($lid->have_complaint === 'no')
                        <a href="/lidsystem/{{$lid->id}}/complaint">Пожаловаться</a>
                    @else
                        @if ($lid->complaint->status === 'moderation')
                            Отправлена на модерацию
                        @elseif ($lid->complaint->status === 'rejected')
                            Отклонена администратором
                        @elseif ($lid->complaint->status === 'accept')
                            Лид забракован администратором
                        @endif
                    @endif
                </div>
            </div>
        @endforeach

        <div class="row">
            <div class="col-8">
                <b>Итого: </b>
            </div>
            <div class="col-2">
                <b>{{$lidCount}} лид</b>
            </div>
            <div class="col-2">
                <b>{{$lidSum}} руб</b>
            </div>
        </div>
    </div>
@endsection
