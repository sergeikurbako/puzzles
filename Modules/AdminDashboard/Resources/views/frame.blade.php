@extends('admindashboard::layouts.master')

@section('content')
    <div class="container">
        <hr>
        <div class="row">
            <form class="" action="/admin-dashboard/frame/{{$frameId}}" method="get">
                <table>
                    <thead>
                        <th>
                            <td><b>Фильтр:</b></td>
                            <td>Дата</td>
                            <td>с <input type="date" name="from_date" value=""></td>
                            <td>по <input type="date" name="to_date" value=""></td>
                            <td><select name="gender">
                                <option value="man">муж</option>
                                <option value="waman">жен</option>
                            </select></td>
                            <td><input type="submit" name="" value="применить"></td>
                        </th>
                    </thead>
                </table>
            </form>

        </div>
        <hr>
        <div class="row">
            <div class="col-1">
                №
            </div>
            <div class="col-1">
                Дата
            </div>
            <div class="col-1">
                ФИО
            </div>
            <div class="col-1">
                Пол
            </div>
            <div class="col-1">
                Возраст
            </div>
            <div class="col-3">
                E-mail
            </div>
            <div class="col-3">
                Номер телефона
            </div>
            <div class="col-1">
                Цена лида
            </div>
        </div>
        @foreach($lids as $lid)
            <hr>
            <div class="row">
                <div class="col-1">
                    {{$lid->id}}
                </div>
                <div class="col-1">
                    {{$lid->created_at}}
                </div>
                <div class="col-1">
                    {{$lid->second_name}} {{$lid->first_name}} {{$lid->patronymic_name}}
                </div>
                <div class="col-1">
                    @if($lid->gender === 'man') муж @else жун @endif
                </div>
                <div class="col-1">
                    {{$lid->age}}
                </div>
                <div class="col-3">
                    {{$lid->email}}
                </div>
                <div class="col-3">
                    {{$lid->phone}}
                </div>
                <div class="col-1">
                    {{$lid->price}}
                </div>
            </div>
        @endforeach

        <div class="row">
            <div class="col-9">
                <b>Итого: </b>
            </div>
            <div class="col-2">
                <b>{{$lidCount}} лид</b>
            </div>
            <div class="col-1">
                <b>{{$lidSum}} руб</b>
            </div>
        </div>
    </div>
@endsection
