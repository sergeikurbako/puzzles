@extends('admindashboard::layouts.master')

@section('content')
    <div class="container">
        <hr>
        <h3>Жалобы</h3>
        <hr>
        <h4>Фильтр</h4>

            <form class="" action="/admin-dashboard/complaints" method="get">
                <div class="row">
                    <div class="col-2">
                        Дата с <input type="date" name="from_date" value=""> по <input type="date" name="to_date" value="">
                    </div>
                    <div class="col-1">
                        Пол
                        <select name="gender">
                            <option value="man">муж</option>
                            <option value="waman">жен</option>
                        </select>
                    </div>
                    <div class="col-3">
                        Статус
                        <select name="status">
                            <option value="moderation">на модерации</option>
                            <option value="rejected">отклонена администратором</option>
                            <option value="accept">забракована администратором</option>
                        </select>
                    </div>
                    <div class="col-3">
                        Цена <br>
                        от <input type="text" name="from_price" value="0"><br>
                        до <input type="text" name="to_price" value="">
                    </div>
                    <div class="col-1">
                        <span>Выгрузить в эксель</span>
                        <input type="checkbox" name="exel" value="yes">
                    </div>
                    <div class="col-1">
                        <input type="submit" name="" value="применить">
                    </div>
                </div>
            </form>

        <hr>
        <div class="row">
            <div class="col-1">
                №
            </div>
            <div class="col-1">
                Дата
            </div>
            <div class="col-1">
                ФИО
            </div>
            <div class="col-1">
                Пол
            </div>
            <div class="col-1">
                Возраст
            </div>
            <div class="col-2">
                E-mail
            </div>
            <div class="col-2">
                Номер телефона
            </div>
            <div class="col-1">
                Цена лида
            </div>
            <div class="col-2">
                Статус заявки
            </div>
        </div>

        @foreach ($lids as $lid)
            <hr>
            <div class="row">
                <div class="col-1">
                    {{$lid->id}}
                </div>
                <div class="col-1">
                    {{$lid->created_at}}
                </div>
                <div class="col-1">
                    {{$lid->second_name}} {{$lid->first_name}} {{$lid->patronymic_name}}
                </div>
                <div class="col-1">
                    @if($lid->gender === 'man') муж @else жун @endif
                </div>
                <div class="col-1">
                    {{$lid->age}}
                </div>
                <div class="col-2">
                    {{$lid->email}}
                </div>
                <div class="col-2">
                    {{$lid->phone}}
                </div>
                <div class="col-1">
                    {{$lid->price}}
                </div>
                <div class="col-2">
                    @if ($lid->complaint->status === 'moderation')
                        Отправлена на модерацию
                    @elseif ($lid->complaint->status === 'rejected')
                        Отклонена администратором
                    @elseif ($lid->complaint->status === 'accept')
                        Лид забракован администратором
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    Сообщение:
                    {{$lid->complaint->message}}
                </div>
                @if ($lid->complaint->status === 'moderation')
                    <form class="" action="/lidsystem/complaints/{{$lid->complaint->id}}/update" method="get">
                        <div class="col-4">
                            Ответ:
                            <input type="text" name="admin_answer" value="">
                            <select name="status">
                                <option value="accept">принять</option>
                                <option value="rejected">отклонить</option>
                            </select>
                            <input type="submit" name="" value="Отправить">
                        </div>
                    </form>
                @else

                @endif
            </div>
        @endforeach
    </div>

@endsection
