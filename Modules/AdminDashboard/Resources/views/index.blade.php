@extends('admindashboard::layouts.master')

@section('content')
    <div class="container">
        <hr>
        <div class="row">
            <div class="col-1">
                №
            </div>
            <div class="col-1">
                Имя
            </div>
            <div class="col-1">
                Лицо
            </div>
            <div class="col-2">
                Дата
            </div>
            <div class="col-3">
                e-mail
            </div>
            <div class="col-1">
                Баланас
            </div>
            <div class="col-2">
                Статус
            </div>
            <div class="col-1">

            </div>
        </div>
        @foreach($users as $user)
            <hr>
            <div class="row">
                <div class="col-1">
                    {{$user->id}}
                </div>
                <div class="col-1">
                    {{$user->userInfo->first_name}}
                </div>
                <div class="col-1">
                    {{$user->userInfo->work_place}}
                </div>
                <div class="col-2">
                    {{$user->created_at}}
                </div>
                <div class="col-3">
                    {{$user->email}}
                </div>
                <div class="col-1">
                    {{$user->balance}}
                </div>
                <div class="col-2">
                    @if ($user->status === 'off')
                        <a href="/admin-dashboard/user/{{$user->id}}/on">Добавить</a>/
                        <a href="/admin-dashboard/user/{{$user->id}}/delete">удалить</a>/
                        остановить
                    @else
                        Добавить/
                        <a href="/admin-dashboard/user/{{$user->id}}/delete">удалить</a>/
                        <a href="/admin-dashboard/user/{{$user->id}}/off">остановить</a>
                    @endif
                </div>
                <div class="col-1">
                    <a href="/admin-dashboard/user/{{$user->id}}">Подробнее</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
