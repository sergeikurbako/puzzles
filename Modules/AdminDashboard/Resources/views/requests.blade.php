@extends('admindashboard::layouts.master')

@section('content')

    <div class="container">
        <hr>
        <div class="row">
            <div class="col-1">
                №
            </div>
            <div class="col-1">
                Игра
            </div>
            <div class="col-4">
                Сайт
            </div>
            <div class="col-2">
                Статус запроса
            </div>
            <div class="col-3">
                Цена за лид
            </div>
        </div>
        @foreach($frames as $frame)
            <hr>
            <div class="row">
                <div class="col-1">
                    {{$frame->id}}
                </div>
                <div class="col-1">
                    {{$frame->game->name}} {{$frame->game->type}}
                </div>
                <div class="col-4">
                    {{$frame->url}}
                </div>
                <div class="col-2">
                    <a href="/gameframe/update-frame-status/{{$frame->id}}/?frame_status=on">Подтвердить</a> / <a href="/gameframe/delete/{{$frame->id}}">Удалить</a>
                </div>
                <div class="col-3">
                    <form class="" action="/gameframe/set-price/{{$frame->id}}" method="post">
                        @csrf
                        <input type="text" name="price" value="{{$frame->price}}">
                        <input type="submit" name="" value="Задать">
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@endsection
