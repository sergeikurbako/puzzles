@extends('admindashboard::layouts.master')

@section('content')

    <div class="container">
        <hr>
        <div class="row">
            <div class="col-1">
                №
            </div>
            <div class="col-1">
                Игра
            </div>
            <div class="col-1">
                Сайт
            </div>
            <div class="col-3">
                Код игры
            </div>
            <div class="col-1">

            </div>
            <div class="col-1">
                SMS подтв.
            </div>
            <div class="col-1">
                email подтв.
            </div>
            <div class="col-1">
                Статус игры
            </div>
            <div class="col-1">
                Статистика
            </div>
            <div class="col-1">
                Цена за лид
            </div>
        </div>
        @foreach($frames as $frame)
            <hr>
            <div class="row">
                <div class="col-1">
                    {{$frame->id}}
                </div>
                <div class="col-1">
                    {{$frame->game->name}} {{$frame->game->type}}
                </div>
                <div class="col-1">
                    {{$frame->url}}
                </div>
                <div class="col-3">
                    {{'<iframe src=\''}}{{stripos($_SERVER["SERVER_PROTOCOL"],"https") === 0 ? "https://" : "http://" . $_SERVER['HTTP_HOST'] . "/lidsystem/?frame_id=" . $frame->id . "&code=" . $frame->code . "' width='1000' height='600'></iframe>"}}
                </div>
                <div class="col-1">
                    <a href="/admin-dashboard/frame/{{$frame->id}}/update">Редактировать</a>
                </div>
                <div class="col-1">
                    @if ($frame->frame_status === 'on')
                        @if($frame->sms_confirm === 'on') Вкл <br> (<a href="/gameframe/update-sms-confirm-status/{{$frame->id}}/?status=off">Выкл</a>) @else Выкл <br> (<a href="/gameframe/update-sms-confirm-status/{{$frame->id}}/?status=on">Вкл</a>) @endif
                    @else
                        Выкл
                    @endif
                </div>
                <div class="col-1">
                    @if ($frame->frame_status === 'on')
                        @if($frame->email_confirm === 'on') Вкл <br> (<a href="/gameframe/update-email-confirm-status/{{$frame->id}}/?status=off">Выкл</a>) @else Выкл <br> (<a href="/gameframe/update-email-confirm-status/{{$frame->id}}/?status=on">Вкл</a>) @endif
                    @else
                        Выкл
                    @endif
                </div>
                <div class="col-1">
                    @if($frame->status === 'on') Вкл <br> (<a href="/gameframe/update-game-status/{{$frame->id}}/?status=off">Выкл</a>) @else Выкл <br>  @endif
                </div>
                <div class="col-1">
                    <a href="/admin-dashboard/frame/{{$frame->id}}">Просмотр</a>
                </div>
                <div class="col-1">
                    <form class="" action="/gameframe/set-price/{{$frame->id}}" method="post">
                        @csrf
                        <input type="text" name="price" value="{{$frame->price}}">
                        <input type="submit" name="" value="Задать">
                    </form>

                </div>
            </div>
        @endforeach

        <hr>
        <a href="/admin-dashboard/create-frame/?user_id={{$userId}}">Создать код игры</a>
        <hr>
    </div>
@endsection
