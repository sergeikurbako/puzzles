<?php

namespace Modules\LidSystem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\LidSystem\Entities\Complaint;
use Modules\LidSystem\Entities\Lid;
use Auth;
use Mail;
use App\Models\User;

class ComplaintController extends Controller
{
    protected $email;

    public function createComplaint(Request $request, int $lidId)
    {
        if (Auth::user()->role !== 'user') {
            return redirect('/login');
        }

        return view('lidsystem::create-complaint', [
            'lidId' => $lidId
        ]);
    }

    public function storeComplaint(Request $request, int $lidId)
    {
        if (Auth::user()->role !== 'user') {
            return redirect('/login');
        }

        $lid = Lid::find($lidId);
        $lid->have_complaint = 'yes';
        $lid->save();

        $message = $request->input('message');

        $complaint = new Complaint;
        $complaint->user_id = Auth::user()->id;
        $complaint->lid_id = $lidId;
        $complaint->message = $message;
        $complaint->save();

        // Отправка сообщения на почту админу
        $admins = User::where('role', 'admin')->get();
        foreach ($admins as $key => $admin) {
            $this->email = $admin->email;
            Mail::send('admindashboard::notifier', ['messages' => 'Поступила жалоба на лид'], function ($m) {
                $m->subject('Поступила жалоба на лид');
                $m->from('partylivea@gmail.com', 'Puzzles');
                $m->to($this->email, $this->email);
            });
        }

        return view('lidsystem::access-complaint');
    }

    public function updateComplaint(Request $request, int $complaintId)
    {
        if (Auth::user()->role !== 'admin') {
            return redirect('/login');
        }

        $complaint = Complaint::find($complaintId);
        $complaint->status = $request->input('status');
        $complaint->admin_answer = $request->input('admin_answer');
        $complaint->save();

        $messages = 'Жалоба по лиду отклонена';
        $user = User::find($complaint->user_id);
        if ($request->input('status') === 'accept') {
            $user->balance += $complaint->lid->price;
            $user->save();

            $messages = 'Жалоба по лиду одобрена';
        }

        $messages .= '. ' . $request->input('admin_answer');

        $this->email = $user->email;
        Mail::send('admindashboard::notifier', ['messages' => $messages], function ($m) {
            $m->subject('Жалоба одобрена');
            $m->from('partylivea@gmail.com', 'Puzzles');
            $m->to($this->email, $this->email);
        });

        return redirect()->back();
    }
}
